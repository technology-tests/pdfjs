Live demo
===

- https://technology-tests.gitlab.io/javascript/pdfjs/web/viewer.html
  - Shows a default PDF file. You can open your own PDF file with the open icon in the toolbar, or with <kbd>ctrl</kbd>+<kbd>o</kbd>. Data is not sent to a remote server
  - [Browse deployed code](https://gitlab.com/technology-tests/javascript/pdfjs/-/jobs/artifacts/gitlab-ci/browse/public/?job=pages)


Changes to [original PDF.js](https://github.com/mozilla/pdf.js)
===

- [`git log v3.0.279...gitlab-ci`](https://gitlab.com/technology-tests/javascript/pdfjs/-/compare/v3.0.279...gitlab-ci?from_project_id=41165245&straight=false)
